<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalzadoApiControler extends Controller
{
    public function index(Request $request) {
        if($request->has('page'))
            $calzados = Calzado::paginate(10);
        else 
            $calzados = Calzado::all();
        return response()->json($calzados);
    }
    
    public function store(Request $request) {
        $calzado = new Calzado;
        $calzado->fill($request->all());
        $calzado->save();
        return response()->json($calzado);
    }

    public function show(Calzado $calzado) {
        return response()->json($calzado);
    }

    public function update(Request $request, Calzado $calzado) {
        $calzado->fill($request->all());
        $calzado->save();
        return response()->json($calzado);
    }

    public function destroy(Calzado $calzado) {
        $calzado->delete();
        return response()->json($calzado);
    }

  
    
}
