<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormaApiControler extends Controller
{
    public function index(Request $request) {
        if($request->has('page'))
            $formas = Forma::paginate(10);
        else 
            $formas = Forma::all();
        return response()->json($formas);
    }
    
    public function all() {
        $formas = Forma::all();
        return response()->json($formas);
    }

    public function store(Request $request) {
    }

    public function show(forma $forma) {
    }

    public function update(Request $request, forma $forma) {
    }

    public function destroy(forma $forma) {
    }
}
