<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PersonaApiControler extends Controller
{
    public function index(Request $request) {
        if($request->has('page'))
            $personas = Persona::paginate(10);
        else 
            $personas = Persona::all();
        return response()->json($personas);

    }
    
    public function all() {
        $personas = Persona::all();
        return response()->json($personas);
    }

    public function store(Request $request) {
    }

    public function show(persona $persona) {
    }

    public function update(Request $request, persona $persona) {
    }

    public function destroy(persona $persona) {
    }
}
