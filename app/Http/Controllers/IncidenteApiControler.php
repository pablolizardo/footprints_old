<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IncidenteApiControler extends Controller
{
    public function index(Request $request) {
        if($request->has('page'))
            $incidentes = Incidente::paginate(10);
        else 
            $incidentes = Incidente::all();
        return response()->json($incidentes);
    }
    
    public function all() {
        $incidentes = Incidente::all();
        return response()->json($incidentes);
    }

    public function store(Request $request) {
    }

    public function show(Incidente $incidente) {
    }

    public function update(Request $request, Incidente $incidente) {
    }

    public function destroy(Incidente $incidente) {
    }
}
