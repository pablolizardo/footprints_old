<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MarcaApiControler extends Controller
{
    public function index(Request $request) {
        if($request->has('page'))
            $marcas = Marca::paginate(10);
        else 
            $marcas = Marca::all();
        return response()->json($marcas);
    }
    
    public function all() {
        $marcas = Marca::all();
        return response()->json($marcas);
    }

    public function store(Request $request) {
    }

    public function show(marca $marca) {
    }

    public function update(Request $request, marca $marca) {
    }

    public function destroy(marca $marca) {
    }
}
