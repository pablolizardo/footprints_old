import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import Marcas from '../data/marcas'
import Colores from '../data/colores'
import formas from '../data/formas'
import tipoFoto from '../data/tipoFoto'
import manoUso from '../data/manoUso'
import personaPartes from '../data/personaPartes'
import tez from '../data/tez'
import nariz from '../data/nariz'
import modus from '../data/modus'
import bocaContorno from '../data/bocaContorno'
import bocaEspesor from '../data/bocaEspesor'
import sexo from '../data/sexo'
import menton from '../data/menton'

import ojoColor from '../data/ojoColor'
import ojoForma from '../data/ojoForma'
import ojoTono from '../data/ojoTono'
import cejaDireccion from '../data/cejaDireccion'
import cejaPilosidad from '../data/cejaPilosidad'
import barbaBigote from '../data/barbaBigote'
import cabelloColor from '../data/cabelloColor'
import cabelloLargo from '../data/cabelloLargo'
import estadoCivil from '../data/estadoCivil'

Vue.use(Vuex)

var state = {
  app: {
    name: 'Huellas',
    version: 2.0,
    author: 'Pablo Lizardo'
  },
  endpoint: 'http://127.0.0.1:8000/api/',
  token: '',
  itemId: 0,
  page: 0,
  lastPage: 999,
  apiUrl: 'http://127.0.0.1:8000/api/',
  formas: formas,
  marcas: Marcas,
  colores: Colores,
  tipoFoto: tipoFoto,
  modus: modus,
  personaPartes: personaPartes,
  tez: tez,
  nariz: nariz,
  mano_uso: manoUso,
  boca_contorno: bocaContorno,
  boca_espesor: bocaEspesor,
  menton: menton,
  sexo: sexo,
  ojo_color: ojoColor,
  ojo_forma: ojoForma,
  ojo_tono: ojoTono,
  ceja_direccion: cejaDireccion,
  ceja_pilosidad: cejaPilosidad,
  barba_bigote: barbaBigote,
  cabello_color: cabelloColor,
  cabello_largo: cabelloLargo,
  estado_civil: estadoCivil,
  links: [{
    url: '/calzados',
    name: 'Calzados'
  }, {
    url: '/incidentes',
    name: 'Incidentes'
  }, {
    url: '/personas',
    name: 'Personas'
  }, {
    url: '/busqueda',
    name: 'Busqueda'
  }],
  sideLinks: [{
    url: '/calzados',
    name: 'Estadisticas'
  }, {
    url: '/incidentes',
    name: 'Mapas'
  }, {
    url: '/personas',
    name: 'Configuracion'
  }]
}

export default new Vuex.Store({
  state
})
