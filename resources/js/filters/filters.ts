Vue.filter('format', date => {
  return moment(date).format('DD-MM-YY')
})
Vue.filter('fromNow', date => {
  return moment(date).fromNow()
})
Vue.filter('isNew', date => {
  return moment(date).isBefore('2018-01-01')
})
Vue.filter('pad', n => {
  n = n + ''
  return n.length >= 5 ? n : new Array(5 - n.length + 1).join('0') + n
})
