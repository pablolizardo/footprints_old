
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import Vuex from 'vuex'
import App from './components/App'
import store from './state'
import router from './router'
import axios from 'axios'

import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'

import Loading from './components/Globals/Loading'

import BtnGroupAcciones from './components/Btns/Acciones'
import BtnGroupPagination from './components/Btns/Pagination'

import CalzadoHuella from './components/Calzados/Huella'
import CalzadoFoto from './components/Calzados/Foto'
import CalzadoMarca from './components/Calzados/Marca'
import CalzadoIncidentes from './components/Calzados/Incidentes'
import CalzadoPersonas from './components/Calzados/Personas'
import CalzadoColor from './components/Calzados/Color'
import CalzadoCreate from './components/Calzados/Create'

import PersonaBadge from './components/Personas/Badge'
import PersonaAvatar from './components/Personas/Avatar'
import PersonaIncidentes from './components/Personas/Incidentes'
import PersonaCalzados from './components/Personas/Calzados'
import PersonaDetalles from './components/Personas/Detalles'
import PersonaFotos from './components/Personas/Fotos'
import PersonaMapa from './components/Personas/Mapa'

import IncidenteModus from './components/Incidentes/Modus'
import GlobalsNumero from './components/Globals/Numero'
// import api from './api';
// let myApi = require("./myApi");
// console.log(myApi.calzados.getOne(1));

import vSelect from 'vue-select'
import sexo from './data/sexo';

Vue.component('v-select', vSelect)

window.axios = axios
axios.defaults.withCredentials = true
axios.defaults.crossDomain = true

var _ = require('lodash')
// require('./assets/styles/app.scss')
require('bootstrap')


Vue.component('btn-group-acciones', BtnGroupAcciones)
Vue.component('btn-group-pagination', BtnGroupPagination)
Vue.component('calzado-huella', CalzadoHuella)
Vue.component('calzado-marca', CalzadoMarca)
Vue.component('calzado-foto', CalzadoFoto)
Vue.component('calzado-incidentes', CalzadoIncidentes)
Vue.component('calzado-personas', CalzadoPersonas)
Vue.component('calzado-color', CalzadoColor)
Vue.component('calzado-create', CalzadoCreate)
Vue.component('incidente-modus', IncidenteModus)
Vue.component('numero', GlobalsNumero)
Vue.component('persona-badge', PersonaBadge)
Vue.component('persona-incidentes', PersonaIncidentes)
Vue.component('persona-calzados', PersonaCalzados)
Vue.component('persona-fotos', PersonaFotos)
Vue.component('persona-avatar', PersonaAvatar)
Vue.component('persona-detalles', PersonaDetalles)
Vue.component('persona-mapa', PersonaMapa)

Vue.component('loading', Loading)

Vue.component('icon', Icon)

Vue.use(require('vue-moment'))
Vue.use(Vuex)

Vue.filter('format', date => { return moment(date).format('DD-MM-YY') })
Vue.filter('fromNow', date => { return moment(date).fromNow() })
Vue.filter('isNew', date => { return moment(date).isBefore('2018-01-01') })
Vue.filter('pad', n => { n = n + '' ; return n.length >= 5 ? n : new Array(5 - n.length + 1).join('0') + n })

Vue.config.productionTip = false

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
      App
    },
    template: '<App/>',
    mounted: function () {
      var vm = this
      window.addEventListener('keyup', function (event) {
        if (event.keyCode == 39) {
          vm.$emit('keyPressed', 1)
        }
        if (event.keyCode == 37) {
          vm.$emit('keyPressed', -1)
        }
      })
    }
  })
  