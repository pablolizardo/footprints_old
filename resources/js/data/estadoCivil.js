export default {
  soltero: 'soltero',
  casado: 'casado',
  divorciado: 'divorciado',
  concuvinato: 'concuvinato',
  viudo: 'viudo',
  sd: 's/d'
}
