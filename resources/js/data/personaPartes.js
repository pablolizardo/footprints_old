export default {
  rostro: 'rostro',
  oreja: 'oreja',
  cuello: 'cuello',
  pecho: 'pecho',
  abdomen: 'abdomen',
  espalda: 'espalda',
  cintura: 'cintura',
  brazo_superior: 'brazo superior',
  brazo_inferior: 'brazo inferior',
  mano: 'mano',
  pierna_superior: 'pierna superior',
  pierna_inferior: 'pierna inferior',
  pie: 'pie'
}
