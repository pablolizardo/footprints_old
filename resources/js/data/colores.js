export default {
  '#007bff': 'blue',
  '#6610f2': 'indigo',
  '#6f42c1': 'purple',
  '#e83e8c': 'pink',
  '#dc3545': 'red',
  '#fd7e14': 'orange',
  '#ffc107': 'yellow',
  '#28a745': 'green',
  '#20c997': 'teal',
  '#17a2b8': 'cyan',
  '#fff': 'white',
  '#6c757d': 'gray',
  '#343a40': 'gray',
  //   '#007bff': 'primary',
  //   '#6c757d': 'secondary',
  //   '#28a745': 'success',
  //   '#17a2b8': 'info',
  //   '#ffc107': 'warning',
  //   '#dc3545': 'danger',
  '#f8f9fa': 'light'
  //   '#343a40': 'dark'
}
