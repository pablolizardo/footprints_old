export default {
  rubio: 'rubio',
  castaño: 'castaño',
  negro: 'negro',
  pelirrojo: 'pelirrojo',
  canoso: 'canoso',
  otros: 'otros',
  sd: 'S/D'
}
