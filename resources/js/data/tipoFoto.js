export default {
  0: 'frente',
  1: 'perfil exterior',
  2: 'perfil interior',
  3: 'posterior',
  4: 'superior',
  5: 'suela'
}
