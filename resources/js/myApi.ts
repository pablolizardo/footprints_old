import axios  from "axios";

export default class myApi  {

    public endpoint: String =  'http://127.0.0.1:8000/api/'

    public calzados () {
        return {
            getOne: (id ) => axios.get( this.endpoint + 'calzados/' + id ),
            getAll: () => axios.get( this.endpoint + 'calzados/' ),
            getPage: (page) => axios.get( this.endpoint + "calzados/?limit=10&offset=" + page * 10 ),
        }
    }
}