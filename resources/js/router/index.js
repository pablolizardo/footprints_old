import Vue from 'vue'
import Router from 'vue-router'

import Main from '../components/Main'
import PersonasIndex from '../components/Personas/Index'
import PersonasShow from '../components/Personas/Show'
import IncidentesIndex from '../components/Incidentes/Index'
import IncidentesShow from '../components/Incidentes/Show'
import CalzadosIndex from '../components/Calzados/Index'
import calzadosShow from '../components/Calzados/Show'
import BusquedaIndex from '../components/Busqueda/Index'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'main', component: Main },
    { path: '/personas', name: 'personas', component: PersonasIndex },
    { path: '/personas/:id', name: 'personasShow', component: PersonasShow },
    { path: '/incidentes', name: 'incidentes', component: IncidentesIndex },
    {
      path: '/incidentes/:id',
      name: 'incidentesShow',
      component: IncidentesShow
    },
    { path: '/calzados', name: 'calzados', component: CalzadosIndex },
    { path: '/calzados/:id', name: 'calzadosShow', component: calzadosShow },
    { path: '/busqueda', name: 'busqueda', component: BusquedaIndex }
  ]
})
