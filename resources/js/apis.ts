import axios from 'axios'
var config = {
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest'
  }
}

export default {
  api: 'http://footprints.test/api/',

  getCalzado(id) {    return axios.get(this.api + 'calzados/' + id, config) },
  getCalzados(page) { 
    if (page) 
      return axios.get(this.api + 'calzados/?page=' + page, config ) 
    else 
      return axios.get(this.api + 'calzados' , config ) 
 },

  getForma(id) {    return axios.get(this.api + 'formas/' + id, config) },
  getFormas(page) { 
    if (page) 
      return axios.get(this.api + 'formas/?page=' + page, config ) 
    else 
      return axios.get(this.api + 'formas' , config ) 
  },

  getMarca(id) {    return axios.get(this.api + 'marcas/' + id, config) },
  getMarcas(page) { 
    if (page) 
      return axios.get(this.api + 'marcas/?page=' + page, config ) 
    else 
      return axios.get(this.api + 'marcas' , config ) 
  },

  updateCalzados() { return '' },

  getPersona(id) {    return axios.get(this.api + 'personas/' + id, config) },
  getPersonas(page) { 
    if (page) 
      return axios.get(this.api + 'personas/?page=' + page, config ) 
    else 
      return axios.get(this.api + 'personas' , config ) 
  },
  
  getPersonaCalzados(id, page) { return axios.get(this.api + "personas/" + id + '/calzados/?page=' + page, config ) },

  getIncidente(id) {    return axios.get(this.api + 'incidentes/' + id, config) },
  getIncidentes(page) { 
    if (page) 
      return axios.get(this.api + 'incidentes/?page=' + page, config ) 
    else 
      return axios.get(this.api + 'incidentes' , config ) 
  },

  getCalzadoIncidentes(calzado_id) { return axios.get( this.api + 'calzados/' + calzado_id + '/incidentes/', config ) }
}
