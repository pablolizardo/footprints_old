<?php

use Illuminate\Database\Seeder;

class FormasTableSeeder extends Seeder
{
    
    public function run()
    {
        $formas = [ 'abstracto', 'ajedrez', 'angulos', 'barras', 'circulos', 'cuadrados', 'cuadricula', 'diagonales', 'espiral', 'formas', 'lineas', 'lisa', 'lunares', 'mezcla', 'ondas', 'ovalos', 'puntos', 'textura', 'triangulos'];
        foreach($formas as $forma) {
            DB::table('formas')->insert([ 'nombre'=> $forma ]) ;
        }
    }
}
