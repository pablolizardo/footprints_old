<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FormasTableSeeder::class);
        $this->call(MarcasTableSeeder::class);
        factory(App\Persona::class, 150)->create();
        factory(App\Calzado::class, 100)->create();
        factory(App\Incidente::class, 150)->create();

    }
}
