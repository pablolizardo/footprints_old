<?php

use Illuminate\Database\Seeder;
// use DB;

class MarcasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $marcas = [ 'adidas', 'altayanta', 'boating', 'cat', 'circa', 'cocacola', 'columbia', 'converse', 'cristobalcolon', 'crocs', 'davor', 'dc', 'diadora', 'etnies', 'fila', 'gola', 'head', 'hushpuppies', 'jaguar', 'johnfoos', 'joma', 'keds', 'kickers', 'lacoste', 'lagear', 'lecoq', 'levis', 'marcas', 'merrel', 'mistral', 'montagne', 'munich', 'newbalance', 'nike', 'northstar', 'oakley', 'oasics', 'olympikus', 'penalty', 'pony', 'puma', 'quiksilver', 'rave', 'reebok', 'reef', 'ripcurl', 'roxy', 'salomon', 'sergiotacchini', 'sismo', 'skechers', 'sperry', 'stone', 'supra', 'timberland', 'toot', 'topper', 'ugg', 'umbro', 'underarmour', 'vans', 'victoria', 'volcom' ];
        foreach($marcas as $marca) {
            DB::table('marcas')->insert([ 'nombre'=> $marca ]) ;
        }

    }
}
