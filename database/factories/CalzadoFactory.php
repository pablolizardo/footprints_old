<?php

use Faker\Generator as Faker;

$factory->define(App\Calzado::class, function (Faker $faker) {
    return [
        'forma_sup' => rand(1,10),
        'forma_inf' => rand(1,10),
        'color' => $faker->hexcolor,
        'marca' => rand(1,10),
        'foto_suela' => 'foto.jpg',
    ];
});
