<?php

use Faker\Generator as Faker;

$factory->define(App\Incidente::class, function (Faker $faker) {
    return [
        'modus_operandi' => $faker->word,
        'persona' => rand(1,150),
        'calzado' => rand(1,100),
        'detalle' => $faker->sentence(3),
    ];
});
