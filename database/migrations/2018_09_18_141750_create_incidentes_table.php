<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentesTable extends Migration {
    
    public function up()
    {
        Schema::create('incidentes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('modus_operandi')->nullable();
            $table->string('created')->nullable();
            $table->string('modified')->nullable();
            $table->integer('persona')->nullable();
            $table->integer('calzado')->nullable();
            $table->string('detalle')->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('incidentes');
    }
}
