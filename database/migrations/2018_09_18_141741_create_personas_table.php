<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration {
    
    public function up() {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre')->nullable();
            $table->string('apellido')->nullable();
            $table->string('apellido2')->nullable();
            $table->string('domicilio')->nullable();
            $table->string('domicilio_laboral')->nullable();
            $table->date('fnac')->nullable();
            $table->float('talle')->nullable();
            $table->integer('registro')->nullable();
            $table->integer('dni')->nullable();
            $table->string('apodo')->nullable();
            $table->string('ocupacion')->nullable();
            $table->string('email')->nullable();
            $table->string('nombre_padre')->nullable();
            $table->boolean('padre_vive')->nullable();
            $table->string('nombre_madre')->nullable();
            $table->boolean('madre_vive')->nullable();
            $table->string('conyugue')->nullable();
            $table->boolean('conyugue_vive')->nullable();
            $table->string('telefono')->nullable();
            $table->string('celular')->nullable();
            $table->string('estado_civil')->nullable();
            $table->string('nacionalidad')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('color')->nullable();
            $table->string('lugar_residencia')->nullable();
            $table->float('altura')->nullable();
            $table->float('peso')->nullable();
            $table->string('tez')->nullable();
            $table->string('ojo_color')->nullable();
            $table->string('ojo_forma')->nullable();
            $table->string('ojo_tono')->nullable();
            $table->string('cabello_color')->nullable();
            $table->string('cabello_largo')->nullable();
            $table->string('barba_bigote')->nullable();
            $table->string('ceja_pilosidad')->nullable();
            $table->string('ceja_direccion')->nullable();
            $table->string('menton')->nullable();
            $table->string('mano_de_uso')->nullable();
            $table->string('boca_contorno')->nullable();
            $table->string('boca_espesor')->nullable();
            $table->string('nariz')->nullable();
            $table->string('sexo')->nullable();
            $table->string('created')->nullable();
            $table->string('modified')->nullable();
            $table->string('avatar')->nullable();

            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('personas');
    }
}
